package main

import "fmt"

/* func main() {
	var x int8
	x = 123
	y := "ashok"
	fmt.Println(x)
	fmt.Printf("x is %T varible is value is %d \n",x,x)
	fmt.Printf("y is %T varible is value is %s \n",y,y)
	y1 := 123
	fmt.Printf("y is %T varible is value is %s \n",y1,y1)
} */


/* func main() {
	var a, b, c = 3, 4, "foo"  
	 
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Printf("a is of type %T\n", a)
	fmt.Printf("b is of type %T\n", b)
	fmt.Printf("c is of type %T\n", c)
 } */


 func main() {
	 var a int,b float32,c string

	 fmt.Printf("a is %T b is %T c is %T",a,b,c)
 }