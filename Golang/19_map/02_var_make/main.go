package main

import "fmt"

func main() {

	var myGreeting = make(map[string]string)
	myGreeting["1"] = "Ashok"
	myGreeting["2"] = "Kumar"

	fmt.Println(myGreeting)
}
