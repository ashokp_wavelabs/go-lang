package main

import "fmt"

func main() {
	var myArry [5]string;
	//var myGreeting map[string]string
	//fmt.Println(myGreeting)
	//fmt.Println(myGreeting == nil)
	fmt.Println(myArry);
}

// add these lines:
/*
	myGreeting["Tim"] = "Good morning."
	myGreeting["Jenny"] = "Bonjour."
*/
// and you will get this:
// panic: assignment to entry in nil map
