package main

import "fmt"

func main() {

	/* mySlice := []int{1, 3, 5, 7, 9, 11} */

	array := [5]int{1, 3, 5, 8, 11}
	 
	fmt.Printf("%T \n", array)
	fmt.Println(array)

	/* fmt.Printf("%T\n", mySlice)
	fmt.Println(mySlice) */

}
